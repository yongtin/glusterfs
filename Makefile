#
# Makefile for building guiderunner
#
PWD := $(shell pwd)

dockerbuild: dockerhealth
	docker build -t registry.gitlab.com/yongtin/glusterfs .

dockerpush: dockerhealth
	docker push registry.gitlab.com/yongtin/glusterfs

dockerhealth:
	@docker ps -q > /dev/null

build: dockerbuild

push: dockerpush

