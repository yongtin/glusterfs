docker run -it --privileged=true \
  -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
  -v /tmp/e:/etc/glusterfs:z \
  -v /tmp/l:/var/log/glusterfs:z \
  -v /tmp/g:/var/lib/glusterd:z \
  -v /dev/:/dev \
  --net=host \
  registry.gitlab.com/yongtin/glusterfs
