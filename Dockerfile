FROM ubuntu:14.04

ENV container docker

MAINTAINER tsang.yong@gmail.com

RUN apt-get update; \
    apt-get install -y wget lsof nfs-common attr iproute2 ntp rsync glusterfs-client glusterfs-common glusterfs-server; 

RUN /etc/init.d/glusterfs-server start; \
    mkdir -p /etc/glusterfs_bkp /var/lib/glusterd_bkp /var/log/glusterfs_bkp;\
    cp -r /etc/glusterfs/* /etc/glusterfs_bkp;\
    cp -r /var/lib/glusterd/* /var/lib/glusterd_bkp;\
    cp -r /var/log/glusterfs/* /var/log/glusterfs_bkp;

ADD glusterfs-server-wrapper.sh /opt/glusterfs-server-wrapper.sh
ADD glusterfs-init-setup.sh /opt/glusterfs-init-setup.sh
RUN chmod 500 /opt/glusterfs-server-wrapper.sh; \
    chmod 500 /opt/glusterfs-init-setup.sh;

VOLUME [ "/sys/fs/cgroup" ]
VOLUME [ "/data" ]

EXPOSE 2222 111 245 443 24007 2049 8080 6010 6011 6012 38465 38466 38468 38469 49152 49153 49154 49156 49157 49158 49159 49160 49161 49162

CMD ["/opt/glusterfs-server-wrapper.sh"]
